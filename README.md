# GoHugo Book Project Template

Template als initiele opzet voor een GoHugo Book Project repository via hergebruik van Go Modules.

Zie de [opzet](SETUP.md) van deze repository

## Nieuwe Project Repository maken

Om een nieuwe Project Repository op basis van dit *Project Template* te maken moeten volgende stappen worden doorlopen:

1. Ga naar [Import project](https://gitlab.com/projects/new#import_project)

2. En kies vervolgens ![](setup/repo-by-url.png)

3. Kopieer de HTTPS URL van dit *Project Template* 

```template
https://gitlab.com/istddevops/shared/gohugo/gohugo-book-project-template.git
```
naar 

![](setup/git-repo-url.png)

4. Geef `groep, benaming en overige eigenschappen` op voor je nieuwe Project Repository ![](setup/group-naam-eigenschappen.png)

5. Maak de nieuwe Project Repository via de ![](setup/create-project.png) button
