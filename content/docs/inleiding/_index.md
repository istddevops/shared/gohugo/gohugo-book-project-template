---
title: Inleiding

---
{{< hint danger >}}
Dit is een **CONCEPT PUBLICATIE** voor demonstratie doeleinden!  
en zo
{{< /hint >}}

{{< kroki name="plantUMLtest" >}}

```plantuml
@startuml
left to right direction
skinparam packageStyle rectangle
skinparam monochrome true
actor customer
actor clerk
rectangle checkout {
  customer -- (checkout)
  (checkout) .> (payment) : include
  (help) .> (checkout) : extends
  (checkout) -- clerk
}
@enduml
```

{{< /kroki >}}