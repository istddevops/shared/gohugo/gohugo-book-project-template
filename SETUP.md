# Opzet GoHugo Book Project Template

Dit GoHugo Project Template is opgezet op basis van Go Modules om te zorgen voor flexibileit in gebruik en uitbreidbaarheid van beschikbare **Themes** en eigen maatwerk daarop. Zie voor achtergrond https://gohugo.io/hugo-modules/use-modules/

## Installatie lokale afhankelijkheden

Om de opzet met Go Modules lokaal te kunnen doorvoeren zijn de volgende CLI-tools geinstalleerd:

- [X] MacOS
  - brew install hugo
  - brew install go
- [ ] Windows

